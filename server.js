const express = require('express');
const app = express();
const admin = require('firebase-admin');
require('dotenv').config();
const serviceAccount = require('./voiture-coding-firebase-adminsdk-6t0qc-bbf3ef7d02.json');
var cors = require('cors')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const bodyParser = require("body-parser");
const {auth} = require("firebase-admin");
const {downquota} = require("./quota");
const jwt = require("jsonwebtoken");

// Content-type: application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors())

app.post("/quota", async (req, res) => {
    const jwtToken = req.body.jwtToken
    const verify = await jwt.verify(jwtToken, process.env.SECRET)
    if (!verify) {
        res.status(403).json({
            error: {
                message: "Unauthorized",
            }
        });
        return;
    }

    const uid = verify.userId
    console.log("uid: ", uid);
    auth.tenantId = uid;
    //recuperer le token de l'utilisateur
    downquota(uid);
    const db = admin.firestore();
    const snapshot = await db.collection('uid').get();
    snapshot.forEach((doc) => {
        console.log(doc.id, '=>', doc.data());
    });
    res.json({quota:snapshot.docs.map(doc => doc.data())});
});

app.listen(3004, () => {
    console.log("Serveur démarré" );
});
