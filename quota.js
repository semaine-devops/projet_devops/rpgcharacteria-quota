const express = require('express');
const app = express();
const admin = require('firebase-admin');
require('dotenv').config();

const bodyParser = require("body-parser");

// Content-type: application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

async function downquota(userID) {
    const db = admin.firestore();
    const snapshot = await db.collection('uid').get();
    function setdata(quotanumber) {
        const docRef = db.collection('uid').doc(userID);
        docRef.set({
            quota: quotanumber - 1
        });
    }

    snapshot.forEach((doc) => {
        console.log(doc.data().quota);
        const quota = doc.data().quota;
        if (quota > 0) {
            setdata(quota);
        }

    });




}

module.exports = {downquota};
