FROM node:18

WORKDIR /usr/src/app

#install dependencies
COPY package.json ./
RUN npm install
COPY . .
EXPOSE 3004
CMD ["",""]
CMD ["npm", "start"]
